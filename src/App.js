import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import Example from './Example';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to A-Schmauseboard</h2>
        </div>
		<div>
			<Example />
		</div>
        <p className="App-intro">
			&copy; Tamara Brandstätter, Norbert Fesel, Valentin Stremnitzer
        </p>
      </div>
    );
  }
}


export default App;
